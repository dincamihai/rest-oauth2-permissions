from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import mixins
from rest_framework import generics
from rest_framework import permissions

from myapp.models import Company
from myapp.models import Project
from myapp.models import ProjectItem
from serializers import CompanySerializer
from serializers import ProjectSerializer
from serializers import ProjectItemSerializer


class UserInCompany(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.is_anonymous():
            return False
        return True

    def has_object_permission(self, request, view, obj):
        if request.user.is_anonymous():
            return False
        if obj:
            if isinstance(obj, Company):
                return bool(obj.users.filter(username=request.user.username).count())
            elif getattr(obj, 'company', None):
                return bool(obj.company.users.filter(username=request.user.username).count())
        return True


class UserIsAdminInProject(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.is_anonymous():
            return False
        return True

    def has_object_permission(self, request, view, obj):
        if request.user.is_anonymous():
            return False
        if obj:
            project = None
            if isinstance(obj, Project):
                project = obj
            elif getattr(obj, 'project', None):
                project = obj.project
            if project:
                return request.user.has_perm('myapp.is_admin', project)
        return True


class CompanyList(generics.ListCreateAPIView):
    model = Company
    serializer_class = CompanySerializer
    permission_classes = (UserInCompany,)


class CompanyDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    permission_classes = (UserInCompany,)


class ProjectList(generics.ListCreateAPIView):
    model = Project
    serializer_class = ProjectSerializer
    permission_classes = (UserInCompany, UserIsAdminInProject)

    def get_queryset(self):
        return Project.objects.filter(
            #user__company=self.kwargs['company'],
            #project=self.kwargs['project']
            )


class ProjectDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    permission_classes = (UserInCompany, UserIsAdminInProject)


class ProjectItemList(generics.ListCreateAPIView):
    model = ProjectItem
    serializer_class = ProjectItemSerializer
    permission_classes = (UserInCompany, UserIsAdminInProject)


class ProjectItemDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ProjectItem.objects.all()
    serializer_class = ProjectItemSerializer
    permission_classes = (UserInCompany, UserIsAdminInProject)

