from django.forms import widgets
from rest_framework import serializers

from myapp.models import Company
from myapp.models import Project
from myapp.models import ProjectItem


class CompanySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Company
        fields = ('name',)


class ProjectSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Project
        fields = ('name', 'company')


class ProjectItemSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ProjectItem
        fields = ('name', 'project')

