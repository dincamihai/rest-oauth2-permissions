from django.conf.urls import patterns, include, url

from views import CompanyList
from views import CompanyDetail
from views import ProjectList
from views import ProjectDetail
from views import ProjectItemList
from views import ProjectItemDetail


urlpatterns = patterns('',
    url(r'^companies/$', CompanyList.as_view(), name='company-list'),
    url(r'^companies/(?P<pk>[0-9]+)/$', CompanyDetail.as_view(), name='company-detail'),
    url(r'^projects/$', ProjectList.as_view(), name='project-list'),
    url(r'^projects/(?P<pk>[0-9]+)/$', ProjectDetail.as_view(), name='project-detail'),
    url(r'^projectitems/$', ProjectItemList.as_view(), name='projectitem-list'),
    url(r'^projectitems/(?P<pk>[0-9]+)/$', ProjectItemDetail.as_view(), name='projectitem-detail'),
)

