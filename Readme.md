## Setup

```
#!bash

$ virtualenv env

$ pip install -r requirements.txt

$ ./manage.py runserver localhost:7000

```


## OAuth2 on Rest API

1. You need to create a Client in http://localhost:7000/admin/oauth2/client/ and copy/paste
Client ID and Client Secret in the cURL call below, together with user's username and password:

   $ curl -X POST -d "client_id=CLIENT_ID&client_secret=CLIENT_SECRET&grant_type=password&username=USERNAME&password=PASSWORD"\
     http://localhost:7000/oauth2/access_token/

That should respond with something very similar to this:

    {
        "access_token": "0b037ebd9bb00837a07f47037fb4e2b73829c901",
        "token_type": "Bearer",
        "expires_in": 31535999,
        "refresh_token": "4f50a6aa75eb08073bc1d4ab51651d5b13575114",
        "scope": "read"
        }

Then, for any other API request, you should always include the **Authorization** HTTP header
with "Bearer " prefix, appended by the **access_token** returned above, like below:

    $ curl -H "Authorization: Bearer 0b037ebd9bb00837a07f47037fb4e2b73829c901"\
      http://localhost:7000/api/companies/1/

The user must be included in such Company in field "users".

## Permissions per project

To make it easier, ensure you called "**make load__initial**" when you installed this project for
the first time, you will have the same data used below.

Take a look at http://localhost:7000/admin/myapp/userprojectpermission/ to see there is one
permissions granted:

- "Is Admin" for "api_user" in "Project 1"

That means the following URL calls will return as below:

First as user "api_user":

    $ curl -H "Authorization: Bearer 41e54f6f68b15128e903f4c1ae1f564a9ed86c88"\
      http://localhost:7000/api/projectitems/1/
    {"name": "Item 1", "project": "http://localhost:7000/api/projects/1/"}

And then with user "other_user":

    $ curl -H "Authorization: Bearer deb5e89d67537dbb6a859c762e89c7d16ea4f02c"\
      http://localhost:7000/api/projectitems/1/
    {"detail": "You do not have permission to perform this action."}