from permission.logics.base import PermissionLogic
from permission.utils.permissions import perm_to_permission


class ProjectPermissionLogic(PermissionLogic):
    def has_perm(self, user_obj, perm, obj=None):
        if user_obj.is_authenticated() and obj is not None:
            permission = perm_to_permission(perm)

            if user_obj.project_permissions.filter(project=obj, permission=permission).count():
                return True

        return False

