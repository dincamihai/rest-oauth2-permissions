from django.db import models
from permission import add_permission_logic
from permission import logics

from permission_logics import ProjectPermissionLogic


class Company(models.Model):
    class Meta:
        ordering = ('name',)

    name = models.CharField(max_length=50)
    users = models.ManyToManyField('auth.user', related_name='companies', blank=True)

    def __unicode__(self):
        return self.name


class Project(models.Model):
    class Meta:
        ordering = ('name',)
        permissions = (
            ('is_admin', 'Is Admin'),
            ('is_evaluator', 'Is Evaluator'),
            ('is_reviewer', 'Is Reviewer'),
            )

    company = models.ForeignKey('Company', related_name='projects')
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name


class ProjectItem(models.Model):
    class Meta:
        ordering = ('name',)

    project = models.ForeignKey('Project', related_name='items')
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name


class UserProjectPermission(models.Model):
    class Meta:
        unique_together = (
            ('user', 'project', 'permission'),
            )

    user = models.ForeignKey('auth.User', related_name='project_permissions')
    project = models.ForeignKey('Project', related_name='user_permissions')
    permission = models.ForeignKey('auth.Permission')


# Apply permissions
add_permission_logic(Company, logics.CollaboratorsPermissionLogic(
    field_name='users',
    #any_permission=True,
    ))
#add_permission_logic(Company, logics.StaffPermissionLogic())
add_permission_logic(Project, ProjectPermissionLogic())

