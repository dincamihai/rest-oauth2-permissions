from django.contrib import admin

from models import Company
from models import Project
from models import ProjectItem
from models import UserProjectPermission


admin.site.register(Company)
admin.site.register(Project)
admin.site.register(ProjectItem)
admin.site.register(UserProjectPermission)

