export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

server:
	./env/bin/python manage.py runserver 0.0.0.0:7000
install:
	./env/bin/pip install -r requirements.txt
db:
	./env/bin/python manage.py syncdb
	./env/bin/python manage.py migrate
shell:
	./env/bin/python manage.py shell
dump_initial:
	./env/bin/python manage.py dumpdata auth > fixtures/initial_01_auth.json
	./env/bin/python manage.py dumpdata myapp > fixtures/initial_02_myapp.json
	./env/bin/python manage.py dumpdata oauth2 > fixtures/initial_03_oauth2.json
load_initial:
	./env/bin/python manage.py loaddata fixtures/*
